import Teams from "./components/Teams.vue";
import Fixtures from "./components/Fixtures.vue";
import Leagues from "./components/Leagues.vue";
import FixturesTeam from "./components/FixturesTeam.vue";
import VueRouter from "vue-router";

export default new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/leagues/:season?",
      name: "Leagues",
      component: Leagues,
    },
    { path: "/teams/:id/:season",
      name: "Teams", 
      component: Teams 
    },
    { path: "/fixtures/:id/:season", 
      name: "Fixtures",
      component: Fixtures 
    },
    {
      path: "/fixture/team/:id/:leagueId/:season",
      name: "FixturesTeam",
      component: FixturesTeam,
    },
    { path: "/",
     redirect: "/leagues" 
    },
  ],
});
